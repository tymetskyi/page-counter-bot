from databases import Database

from bot import settings

database = Database(settings.DATABASE_URL)


def db_connection(func):
    """Decorator which provide db connection."""

    async def wrapper(*args, **kwargs):
        await database.connect()
        result = await func(*args, **kwargs)
        await database.disconnect()
        return result

    return wrapper


@db_connection
async def run_db():
    """Create user table."""
    query = """
        CREATE TABLE IF NOT EXISTS "User" (
            user_id INTEGER PRIMARY KEY,
            first_name VARCHAR(100),
            goal INTEGER NOT NULL,
            book_pages INTEGER ARRAY NOT NULL
        );
    """
    await database.execute(query=query)


@db_connection
async def get_user_by_id(user_id):
    """Get and return user by id."""
    await database.connect()
    query = 'SELECT * FROM "User" WHERE user_id = :user_id;'
    user = await database.fetch_one(query, values={"user_id": user_id})
    return user


@db_connection
async def create_user(*, user_id: int, first_name: str, goal: int, book_pages: list):
    """Create user in db."""
    query = """
        INSERT INTO "User" (user_id, first_name, goal, book_pages)
        VALUES (:user_id, :first_name, :goal, :book_pages);
    """
    await database.execute(
        query,
        values={
            "user_id": user_id,
            "first_name": first_name,
            "goal": goal,
            "book_pages": book_pages,
        },
    )


@db_connection
async def add_user_book(*, user_id: int, pages: int):
    query = """
        UPDATE "User"
        SET book_pages = array_append(book_pages, :pages)
        WHERE user_id = :user_id
    """
    await database.execute(query, values={"user_id": user_id, "pages": pages})


@db_connection
async def clean_user_books(user_id: int):
    query = """
        UPDATE "User"
        SET book_pages = '{}'
        WHERE user_id = :user_id;
    """
    await database.execute(query, values={"user_id": user_id})


@db_connection
async def update_user_goal(*, user_id: int, goal: int):
    query = """
        UPDATE "User"
        SET goal = :goal
        WHERE user_id = :user_id;
    """
    await database.execute(query, values={"user_id": user_id, "goal": goal})
