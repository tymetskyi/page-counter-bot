from datetime import datetime

DAY_IN_YEAR = 365
DAY_OF_YEAR = datetime.now().timetuple().tm_yday
DAY_TO_THE_END_OF_YEAR = DAY_IN_YEAR - DAY_OF_YEAR


class BookStatisticsManager:
    def __init__(self, goal: int, book_pages: list) -> None:
        self.goal = goal
        self.book_pages = book_pages
        self.read_books = len(self.book_pages)  # already read
        self.left_to_read = self.goal - self.read_books

    def __len__(self) -> int:
        # return sum of input book pages
        # if self.on_coefficient:
        #     total_pages = 0
        #     for pages, coefficient in self.book_pages.items():
        #         total_pages += pages * coefficient
        #     return int(total_pages)
        return sum(self.book_pages)

    def get_average_pages_per_book(self) -> float:
        total_pages = len(self)  # sum of pages
        avg_pages_per_book = total_pages / self.read_books
        return avg_pages_per_book

    def get_daily_statistics(self) -> tuple:
        avg_pages_per_book = self.get_average_pages_per_book()
        pages_per_day = (
            avg_pages_per_book * self.left_to_read
        ) / DAY_TO_THE_END_OF_YEAR
        return round(avg_pages_per_book), f"{pages_per_day:.2f}"

    def generate_msg(self) -> str:
        if not self.read_books:
            return "You don't have enough book to see any statistics."

        avg_pages_per_book, pages_per_day = self.get_daily_statistics()
        msg = f"""
        Goal: {self.goal} 📕
        Already read: {self.read_books} 📘
        Average pages per book: {avg_pages_per_book}
        Need to read pages per day: {pages_per_day}
        """
        return msg
