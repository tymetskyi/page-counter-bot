from enum import Enum


class MessageEnum(str, Enum):
    new_user = 'You are new user, please try to press "/start" at first.'
    tiny_statistics = "You don't have enough book to see any statistics. Please add your books and then try again."
    stored = "Stored."
