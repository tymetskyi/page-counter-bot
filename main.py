import logging

from aiogram import Bot, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import Dispatcher, FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.executor import start_webhook

from bot import settings
from bot.db import (
    add_user_book,
    clean_user_books,
    create_user,
    get_user_by_id,
    run_db,
    update_user_goal,
)
from bot.services import BookStatisticsManager
from bot.enums import MessageEnum

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Initialize bot and dispatcher
bot = Bot(token=settings.API_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


class StartForm(StatesGroup):
    """Form for pool."""

    goal = State()
    book_pages = State()


class AddBookForm(StatesGroup):
    """Add book form."""

    pages = State()


class UpdateGoalForm(StatesGroup):
    """Update goal form."""

    goal = State()


async def on_startup(dispatcher):
    await bot.set_webhook(settings.WEBHOOK_URL, drop_pending_updates=True)
    await run_db()  # create tables


async def on_shutdown(dispatcher):
    await bot.delete_webhook()


@dp.message_handler(commands=["start"])
async def start(message: types.Message):
    logging.info(f"New start at {message.date}. Data: {message}")

    user = await get_user_by_id(message.from_user.id)
    if user:
        await message.answer(f"Hi, {user.first_name}.")
    else:
        await StartForm.goal.set()  # ask the goal
        await message.answer("Please set your book annual goal.")


@dp.message_handler(lambda message: message.text.isdigit(), state=StartForm.goal)
async def set_goal(message: types.Message, state: FSMContext):
    """Process user goal."""
    await state.update_data(user_goal=int(message.text))
    await message.answer(f"OMG, what the incredible goal ({message.text})!")

    await StartForm.next()  # ask the book pages
    await message.answer("")


@dp.message_handler(state=StartForm.book_pages)
async def set_books(message: types.Message, state: FSMContext):
    """Process book pages."""
    book_pages = list(map(int, message.text.split(",")))
    await state.update_data(user_book_pages=book_pages)

    async with state.proxy() as data:
        await create_user(
            user_id=message.from_user.id,
            first_name=message.from_user.first_name,
            goal=data["user_goal"],
            book_pages=data["user_book_pages"],
        )
        await message.answer("All set! User was created.")

    logger.info(f"Start flow data: {data}")
    await state.finish()  # finish the poll.


@dp.message_handler(commands=["get_statistics"])
async def get_statistics(message: types.Message):
    user = await get_user_by_id(message.from_user.id)
    if user is None:
        return await message.answer(MessageEnum.new_user.value)

    if not user.book_pages:
        return await message.answer(MessageEnum.tiny_statistics.value)

    book_statistics = BookStatisticsManager(goal=user.goal, book_pages=user.book_pages)
    msg = book_statistics.generate_msg()
    await message.answer(f"Dear {user.first_name}, here is your statistics:" + msg)


@dp.message_handler(commands=["clean_books"])
async def clean_books(message: types.Message):
    user = await get_user_by_id(message.from_user.id)
    if user is None:
        await message.answer(MessageEnum.new_user.value)

    # clean book
    await clean_user_books(message.from_user.id)
    await message.answer("Books was deleted.")


@dp.message_handler(commands=["add_book"])
async def add_book(message: types.Message):
    user = await get_user_by_id(message.from_user.id)
    if user is None:
        await message.answer(MessageEnum.new_user.value)

    await AddBookForm.pages.set()  # ask the goal
    await message.answer("How many pages have you read?")


@dp.message_handler(lambda message: message.text.isdigit(), state=AddBookForm.pages)
async def set_book_pages(message: types.Message, state: FSMContext):
    await add_user_book(user_id=message.from_user.id, pages=int(message.text))
    await message.answer(MessageEnum.stored.value)
    await state.finish()  # finish the poll.


@dp.message_handler(commands=["update_goal"])
async def update_goal(message: types.Message):
    user = await get_user_by_id(message.from_user.id)
    if user is None:
        await message.answer(MessageEnum.new_user.value)

    await UpdateGoalForm.goal.set()  # ask the goal
    await message.answer("What is your new goal?")


@dp.message_handler(lambda message: message.text.isdigit(), state=UpdateGoalForm.goal)
async def set_new_goal(message: types.Message, state: FSMContext):
    await update_user_goal(user_id=message.from_user.id, goal=int(message.text))
    await message.answer(MessageEnum.stored.value)
    await state.finish()  # finish the poll.


if __name__ == "__main__":
    start_webhook(
        dispatcher=dp,
        webhook_path=settings.WEBHOOK_PATH,
        skip_updates=True,
        on_startup=on_startup,
        on_shutdown=on_shutdown,
        host=settings.WEBAPP_HOST,
        port=settings.WEBAPP_PORT,
    )
